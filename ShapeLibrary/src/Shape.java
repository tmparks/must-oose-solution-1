
public abstract class Shape
{

    public Shape()
    {
        center = new Point();
    }

    public Point getCenter()
    {
        return center;
    }

    public void translate(double x, double y)
    {
        center.translate(x, y);
    }

    public abstract void rotate(double angle);

    public abstract void scale(double scale);

    public abstract double getArea();

    public abstract double getPerimeter();

    public abstract Rectangle getBounds();

    protected Point center;
}
