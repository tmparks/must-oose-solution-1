
import java.util.List;

public abstract class Polygon extends Shape
{

    @Override
    public double getPerimeter()
    {
        double perimeter = 0.0;
        if (getVertices().size() > 1)
        {
            List<Point> vertices = getVertices();
            Point first = vertices.get(0);
            Point last = first;
            Point prev = first;
            for (int i = 1; i < vertices.size(); i++)
            {
                Point vertex = vertices.get(i);
                perimeter += prev.getDistance(vertex);
                last = prev = vertex;
            }
            perimeter += last.getDistance(first);
        }
        return perimeter;
    }

    @Override
    public Rectangle getBounds()
    {
        double minX = Double.MAX_VALUE;
        double maxX = Double.MIN_VALUE;
        double minY = Double.MAX_VALUE;
        double maxY = Double.MIN_VALUE;
        for (Point vertex : getVertices())
        {
            minX = Double.min(minX, vertex.getX());
            maxX = Double.max(maxX, vertex.getX());
            minY = Double.min(minY, vertex.getY());
            maxY = Double.max(maxY, vertex.getY());
        }
        Rectangle bounds = new Rectangle(maxX - minX, maxY - minY);
        bounds.translate((minX + maxX) / 2.0, (minY + maxY) / 2.0);
        return bounds;
    }

    public abstract List<Point> getVertices();
}
