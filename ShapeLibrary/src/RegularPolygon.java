
import java.util.*;

public class RegularPolygon extends SimplePolygon
{

    public RegularPolygon(double radius, int numVertices)
    {
        this.radius = radius;
        this.numVertices = numVertices;
    }

    public void addPoint()
    {
        numVertices++;
    }

    public void removePoint()
    {
        numVertices--;
    }

    @Override
    public List<Point> getVertices()
    {
        LinkedList<Point> vertices = new LinkedList<>();
        for (int i = 0; i < numVertices; i++)
        {
            double angle = (2.0 * Math.PI * i) / numVertices;
            Point vertex = Point.polar(radius, angle);
            vertex.rotate(orientation);
            vertex.translate(center.getX(), center.getY());
            vertices.add(vertex);
        }
        return vertices;
    }

    @Override
    public void scale(double scale)
    {
        radius *= scale;
    }

    @Override
    public double getArea()
    {
        // Calculate area of isosceles triangle.
        double angle = (2.0 * Math.PI) / numVertices;
        double area = 0.5 * radius * radius * Math.sin(angle);
        return numVertices * area;
    }

    @Override
    public double getPerimeter()
    {
        // Calculate the base of isoceles triangle.
        double angle = (2.0 * Math.PI) / numVertices;
        double length = 2.0 * radius * Math.sin(0.5 * angle);
        return numVertices * length;
    }

    protected double radius;
    protected int numVertices;
}
