
import java.util.*;

public class Rectangle extends SimplePolygon
{

    public Rectangle(double width, double height)
    {
        this.width = width;
        this.height = height;
    }

    @Override
    public void scale(double scale)
    {
        width *= scale;
        height *= scale;
    }

    @Override
    public double getArea()
    {
        return width * height;
    }

    @Override
    public double getPerimeter()
    {
        return 2.0 * (width + height);
    }

    @Override
    public List<Point> getVertices()
    {
        LinkedList<Point> vertices = new LinkedList<>();
        double x = 0.5 * width;
        double y = 0.5 * height;
        vertices.add(Point.rectangular(-x, -y));
        vertices.add(Point.rectangular(+x, -y));
        vertices.add(Point.rectangular(+x, +y));
        vertices.add(Point.rectangular(-x, +y));
        for (Point vertex : vertices)
        {
            vertex.rotate(orientation);
            vertex.translate(center.getX(), center.getY());
        }
        return vertices;
    }

    protected double width;
    protected double height;
}
