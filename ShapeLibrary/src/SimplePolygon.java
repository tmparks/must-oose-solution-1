
public abstract class SimplePolygon extends Polygon
{

    @Override
    public void rotate(double angle)
    {
        orientation += angle;
    }

    public double getOrientation()
    {
        return orientation;
    }

    protected double orientation;
}
