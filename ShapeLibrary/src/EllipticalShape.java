
public abstract class EllipticalShape extends Shape
{

    @Override
    public void scale(double scale)
    {
        semiMajorAxis *= scale;
    }

    protected double semiMajorAxis;

}
