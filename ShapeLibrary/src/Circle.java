
public class Circle extends EllipticalShape
{

    public double getRadius()
    {
        return semiMajorAxis;
    }

    @Override
    public void rotate(double angle)
    {
        // Do nothing.
    }

    @Override
    public double getArea()
    {
        return Math.PI * getRadius() * getRadius();
    }

    @Override
    public double getPerimeter()
    {
        return 2.0 * Math.PI * getRadius();
    }

    @Override
    public Rectangle getBounds()
    {
        Rectangle bounds = new Rectangle(2.0 * getRadius(), 2.0 * getRadius());
        bounds.translate(center.getX(), center.getY());
        return bounds;
    }
}
