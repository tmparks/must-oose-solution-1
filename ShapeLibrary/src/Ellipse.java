
public class Ellipse extends EllipticalShape
{

    @Override
    public void rotate(double angle)
    {
        orientation += angle;
    }

    @Override
    public void scale(double scale)
    {
        super.scale(scale);
        semiMinorAxis *= scale;
    }

    @Override
    public double getArea()
    {
        return Math.PI * semiMajorAxis * semiMinorAxis;
    }

    @Override
    public double getPerimeter()
    {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Rectangle getBounds()
    {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    protected double semiMinorAxis;
    protected double orientation;
}
