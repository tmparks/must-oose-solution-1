
/**
 * Representation of geometric point using both rectangular (Cartesian) and
 * polar coordinates.
 */
public class Point
{

    /**
     * Creates a new point using rectangular coordinates.
     *
     * @param x X coordinate.
     * @param y Y coordinate.
     * @return Point with specified coordinates.
     */
    public static Point rectangular(double x, double y)
    {
        Point p = new Point();
        p.x = x;
        p.y = y;
        p.updatePolar();
        return p;
    }

    /**
     * Creates a new point using polar coordinates.
     *
     * @param radius Radius.
     * @param angle Angle, in radians.
     * @return Point with specified coordinates.
     */
    public static Point polar(double radius, double angle)
    {
        Point p = new Point();
        p.radius = radius;
        p.angle = angle;
        p.updateRectangular();
        return p;
    }

    /**
     * Moves (translates) the point.
     *
     * @param x Horizontal displacement.
     * @param y Vertical displacement.
     */
    public void translate(double x, double y)
    {
        this.x += x;
        this.y += y;
        updatePolar();
    }

    /**
     * Rotates the point about the origin. This alters the point's angle in the
     * polar coordinate system, leaving the radius unchanged.
     *
     * @param angle Angle, in radians.
     */
    public void rotate(double angle)
    {
        this.angle += angle;
        updateRectangular();
    }

    /**
     * Scales the point's radius in the polar coordinate system, leaving the
     * angle unchanged.
     *
     * @param scale Scaling factor. Factors less than 1.0 move the point closer
     * to the origin. Factors larger than 1.0 move the point away from the
     * origin.
     */
    public void scale(double scale)
    {
        this.radius *= scale;
        updateRectangular();
    }

    /**
     * Gets the X portion of the point's rectangular coordinates.
     *
     * @return X coordinate.
     */
    public double getX()
    {
        return x;
    }

    /**
     * Gets the Y portion of the point's rectangular coordinates.
     *
     * @return Y coordinate.
     */
    public double getY()
    {
        return y;
    }

    /**
     * Gets the radius of the point's polar coordinates.
     *
     * @return Radius.
     */
    public double getRadius()
    {
        return radius;
    }

    /**
     * Gets the angle of the point's polar coordinates.
     *
     * @return Angle, in radians.
     */
    public double getAngle()
    {
        return angle;
    }

    /**
     * Gets the distance between this point and the specified point.
     *
     * @param that Point.
     * @return Distance between points.
     */
    public double getDistance(Point that)
    {
        double deltaX = this.x - that.x;
        double deltaY = this.y - that.y;
        return Math.sqrt(deltaX * deltaX + deltaY * deltaY);
    }

    /**
     * Provides the point's rectangular and polar coordinates as a string.
     *
     * @return String with point's coordinates.
     */
    @Override
    public String toString()
    {
        return "Cartesian (" + getX() + ", " + getY() + ")\n"
                + "Polar (" + getRadius() + ", " + getAngle() + ")\n";
    }

    /**
     * Updates the point's rectangular coordinates using the current polar
     * coordinates.
     */
    protected void updateRectangular()
    {
        x = radius * Math.cos(angle);
        y = radius * Math.sin(angle);
    }

    /**
     * Updates the point's polar coordinates using the current rectangular
     * coordinaates.
     */
    protected void updatePolar()
    {
        radius = Math.sqrt(x * x + y * y);
        angle = Math.atan2(y, x);
    }

    /**
     * X coordinate in rectangular coordinate system.
     */
    protected double x;

    /**
     * Y coordinate in rectangular coordinate system.
     */
    protected double y;

    /**
     * Radius in polar coordinate system.
     */
    protected double radius;

    /**
     * Angle in polar coordinate system.
     */
    protected double angle;
}
