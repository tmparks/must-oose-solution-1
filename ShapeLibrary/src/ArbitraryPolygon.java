
import java.util.*;

public class ArbitraryPolygon extends Polygon
{

    public ArbitraryPolygon()
    {
        this.vertices = new LinkedList<>();
    }

    public ArbitraryPolygon(List<Point> vertices)
    {
        this.vertices = new LinkedList<>(vertices);
    }

    public void addPoint(Point vertex)
    {
        vertices.add(vertex);
        center = getBounds().getCenter();
    }

    public Point removePoint(int index)
    {
        Point vertex = vertices.remove(index);
        center = getBounds().getCenter();
        return vertex;
    }

    public Point removePoint()
    {
        return removePoint(vertices.size() - 1);
    }

    @Override
    public List<Point> getVertices()
    {
        return vertices;
    }

    @Override
    public void rotate(double angle)
    {
        for (Point vertex : vertices)
        {
            vertex.translate(-center.getX(), -center.getY());   // Translate to origin.
            vertex.rotate(angle);
            vertex.translate(center.getX(), center.getY());     // Translate back.
        }
    }

    @Override
    public void scale(double scale)
    {
        for (Point vertex : vertices)
        {
            vertex.translate(-center.getX(), -center.getY());   // Translate to origin.
            vertex.scale(scale);
            vertex.translate(center.getX(), center.getY());     // Translate back.
        }
    }

    @Override
    public double getArea()
    {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    protected List<Point> vertices;
}
