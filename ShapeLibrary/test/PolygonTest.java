
import java.util.LinkedList;
import java.util.List;
import org.junit.Test;
import static org.junit.Assert.*;

public class PolygonTest
{

    /**
     * Test of getPerimeter method, of class Polygon.
     */
    @Test
    public void testGetPerimeter()
    {
        System.out.println("getPerimeter");
        Polygon instance = new PolygonImpl();
        double expected = 4.0 * Math.sqrt(2.0);
        double result = instance.getPerimeter();
        assertEquals(expected, result, 0.0);
    }

    /**
     * Test of getBounds method, of class Polygon.
     */
    @Test
    public void testGetBounds()
    {
        System.out.println("getBounds");
        double delta = 1e-6;
        Polygon instance = new PolygonImpl();
        Rectangle expected = new Rectangle(2.0, 2.0);
        Rectangle result = instance.getBounds();
        assertEquals(expected.width, result.width, delta);
        assertEquals(expected.height, result.height, delta);
        assertEquals(expected.center.x, result.center.x, delta);
        assertEquals(expected.center.y, result.center.y, delta);
    }

    public class PolygonImpl extends Polygon
    {

        /**
         * Gets vertices of test polygon.
         *
         * @return Vertices of a diamond.
         */
        @Override
        public List<Point> getVertices()
        {
            LinkedList<Point> vertices = new LinkedList<>();
            vertices.add(Point.rectangular(+1.0, 0.0));
            vertices.add(Point.rectangular(0.0, +1.0));
            vertices.add(Point.rectangular(-1.0, 0.0));
            vertices.add(Point.rectangular(0.0, -1.0));
            return vertices;
        }

        @Override
        public void rotate(double angle)
        {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        @Override
        public void scale(double scale)
        {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        @Override
        public double getArea()
        {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }
    }

}
