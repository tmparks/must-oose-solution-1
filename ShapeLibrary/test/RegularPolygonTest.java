
import java.util.List;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Tom
 */
public class RegularPolygonTest
{

    /**
     * Test of addPoint method, of class RegularPolygon.
     */
    @Test
    public void testAddPoint()
    {
        System.out.println("addPoint");
        double radius = 37.0;
        int numVertices = 2;
        RegularPolygon instance = new RegularPolygon(radius, numVertices);
        instance.addPoint();
        assertEquals(numVertices + 1, instance.getVertices().size());
    }

    /**
     * Test of removePoint method, of class RegularPolygon.
     */
    @Test
    public void testRemovePoint()
    {
        System.out.println("removePoint");
        double radius = 37.0;
        int numVertices = 3;
        RegularPolygon instance = new RegularPolygon(radius, numVertices);
        instance.removePoint();
        assertEquals(numVertices - 1, instance.getVertices().size());
    }

    /**
     * Test of getVertices method, of class RegularPolygon.
     */
    @Test
    public void testGetVertices()
    {
        System.out.println("getVertices");
        double radius = 1.0;
        int numVertices = 4;
        double delta = 1e-6;
        RegularPolygon instance = new RegularPolygon(radius, numVertices);
        List<Point> result = instance.getVertices();
        assertEquals(numVertices, result.size());
        for (Point vertex : result)
        {
            assertEquals(radius, vertex.getRadius(), delta);
        }
    }

    /**
     * Test of scale method, of class RegularPolygon.
     */
    @Test
    public void testScale()
    {
        System.out.println("scale");
        double radius = 1.0;
        int numVertices = 4;
        double scale = 2.0;
        double delta = 1e-6;
        RegularPolygon instance = new RegularPolygon(radius, numVertices);
        instance.scale(scale);
        for (Point vertex : instance.getVertices())
        {
            assertEquals(radius * scale, vertex.getRadius(), delta);
        }
    }

    /**
     * Test of getArea method, of class RegularPolygon.
     */
    @Test
    public void testGetArea()
    {
        System.out.println("getArea");
        double radius = 17.0;
        int numVertices = 4;
        double delta = 1e-6;
        RegularPolygon instance = new RegularPolygon(radius, numVertices);
        double expected = 2.0 * radius * radius;
        double result = instance.getArea();
        assertEquals(expected, result, delta);
    }

    /**
     * Test of getPerimeter method, of class RegularPolygon.
     */
    @Test
    public void testGetPerimeter()
    {
        System.out.println("getPerimeter");
        double radius = 17.0;
        int numVertices = 4;
        double delta = 1e-6;
        RegularPolygon instance = new RegularPolygon(radius, numVertices);
        double expected = 4.0 * Math.sqrt(2.0) * radius;
        double result = instance.getPerimeter();
        assertEquals(expected, result, delta);
    }

}
