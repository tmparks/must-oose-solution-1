
import java.util.List;
import org.junit.Test;
import static org.junit.Assert.*;

public class RectangleTest
{

    /**
     * Test of scale method, of class Rectangle.
     */
    @Test
    public void testScale()
    {
        System.out.println("scale");
        double scale = 0.5;
        double width = 17.0;
        double height = 42.0;
        double delta = 0.0;
        Rectangle instance = new Rectangle(width, height);
        instance.scale(scale);
        assertEquals(instance.center.x, 0.0, delta);
        assertEquals(instance.center.y, 0.0, delta);
        assertEquals(instance.orientation, 0.0, delta);
        assertEquals(width * scale, instance.width, delta);
        assertEquals(height * scale, instance.height, delta);
    }

    /**
     * Test of getArea method, of class Rectangle.
     */
    @Test
    public void testGetArea()
    {
        System.out.println("getArea");
        double width = 17.0;
        double height = 42.0;
        Rectangle instance = new Rectangle(width, height);
        double expected = width * height;
        double result = instance.getArea();
        assertEquals(expected, result, 0.0);
    }

    /**
     * Test of getPerimeter method, of class Rectangle.
     */
    @Test
    public void testGetPerimeter()
    {
        System.out.println("getPerimeter");
        double width = 17.0;
        double height = 42.0;
        Rectangle instance = new Rectangle(width, height);
        double expected = 2.0 * (width + height);
        double result = instance.getPerimeter();
        assertEquals(expected, result, 0.0);
    }

    /**
     * Test of getVertices method, of class Rectangle.
     */
    @Test
    public void testGetVertices()
    {
        System.out.println("getVertices");
        double width = 2.0;
        double height = 2.0;
        double delta = 1e-6;
        Rectangle instance = new Rectangle(width, height);
        List<Point> result = instance.getVertices();
        assertEquals(4, result.size());
        for (Point vertex : result)
        {
            assertEquals(width / 2.0, Math.abs(vertex.getX()), delta);
            assertEquals(height / 2.0, Math.abs(vertex.getY()), delta);
        }
    }

}
