
import org.junit.Test;
import static org.junit.Assert.*;

public class PointTest
{

    /**
     * Test of rectangular method, of class Point.
     */
    @Test
    public void testRectangular()
    {
        System.out.println("rectangular");
        double x = 1.0;
        double y = 1.0;
        double radius = Math.sqrt(2.0);
        double angle = Math.PI / 4.0;
        double delta = 0.0;
        Point result = Point.rectangular(x, y);
        assertEquals(x, result.getX(), delta);
        assertEquals(y, result.getY(), delta);
        assertEquals(radius, result.getRadius(), delta);
        assertEquals(angle, result.getAngle(), delta);
    }

    /**
     * Test of polar method, of class Point.
     */
    @Test
    public void testPolar()
    {
        System.out.println("polar");
        double x = 1.0;
        double y = 1.0;
        double radius = Math.sqrt(2.0);
        double angle = Math.PI / 4.0;
        double delta = 1e-6;
        Point result = Point.polar(radius, angle);
        assertEquals(x, result.getX(), delta);
        assertEquals(y, result.getY(), delta);
        assertEquals(radius, result.getRadius(), delta);
        assertEquals(angle, result.getAngle(), delta);
    }

    /**
     * Test of translate method, of class Point.
     */
    @Test
    public void testTranslate()
    {
        System.out.println("translate");
        double x = 42.0;
        double y = -17.0;
        double dx = -19.0;
        double dy = 13.0;
        double delta = 0.0;
        Point instance = Point.rectangular(x, y);
        instance.translate(dx, dy);
        assertEquals(x + dx, instance.getX(), delta);
        assertEquals(y + dy, instance.getY(), delta);
    }

    /**
     * Test of rotate method, of class Point.
     */
    @Test
    public void testRotate()
    {
        System.out.println("rotate");
        double radius = 42.0;
        double angle = -17.0;
        double rotation = 19.0;
        double delta = 0.0;
        Point instance = Point.polar(radius, angle);
        instance.rotate(rotation);
        assertEquals(radius, instance.getRadius(), delta);
        assertEquals(angle + rotation, instance.getAngle(), delta);
    }

    /**
     * Test of scale method, of class Point.
     */
    @Test
    public void testScale()
    {
        System.out.println("scale");
        double radius = 42.0;
        double angle = -17.0;
        double scale = 19.0;
        double delta = 0.0;
        Point instance = Point.polar(radius, angle);
        instance.scale(scale);
        assertEquals(radius * scale, instance.getRadius(), delta);
        assertEquals(angle, instance.getAngle(), delta);
    }

    /**
     * Test of getX method, of class Point.
     */
    @Test
    public void testGetX()
    {
        System.out.println("getX");
        double expected = 42.0;
        Point instance = Point.rectangular(expected, expected);
        assertEquals(expected, instance.getX(), 0.0);
    }

    /**
     * Test of getY method, of class Point.
     */
    @Test
    public void testGetY()
    {
        System.out.println("getY");
        double expected = 42.0;
        Point instance = Point.rectangular(expected, expected);
        assertEquals(expected, instance.getY(), 0.0);
    }

    /**
     * Test of getRadius method, of class Point.
     */
    @Test
    public void testGetRadius()
    {
        System.out.println("getRadius");
        double expected = 42.0;
        Point instance = Point.polar(expected, expected);
        assertEquals(expected, instance.getRadius(), 0.0);
    }

    /**
     * Test of getAngle method, of class Point.
     */
    @Test
    public void testGetAngle()
    {
        System.out.println("getAngle");
        double expected = 42.0;
        Point instance = Point.polar(expected, expected);
        assertEquals(expected, instance.getAngle(), 0.0);
    }

    /**
     * Test of toString method, of class Point.
     */
    @Test
    public void testToString()
    {
        System.out.println("toString");
        Point instance = new Point();
        String expected = "Cartesian (0.0, 0.0)\nPolar (0.0, 0.0)\n";
        String result = instance.toString();
        assertEquals(expected, result);
    }

    /**
     * Test of updateRectangular method, of class Point.
     */
    @Test
    public void testUpdateRectangular()
    {
        System.out.println("updateRectangular");
        Point instance = new Point();
        instance.radius = Math.sqrt(2.0);
        instance.angle = Math.PI / 4.0;
        double delta = 1e-6;
        instance.updateRectangular();
        assertEquals(1.0, instance.x, delta);
        assertEquals(1.0, instance.y, delta);
    }

    /**
     * Test of updatePolar method, of class Point.
     */
    @Test
    public void testUpdatePolar()
    {
        System.out.println("updatePolar");
        Point instance = new Point();
        instance.x = 1.0;
        instance.y = 1.0;
        double delta = 1e-6;
        instance.updatePolar();
        assertEquals(Math.sqrt(2.0), instance.radius, delta);
        assertEquals(Math.PI / 4.0, instance.angle, delta);
    }

    /**
     * Test of getDistance method, of class Point.
     */
    @Test
    public void testGetDistance()
    {
        System.out.println("getDistance");
        Point that = new Point();
        Point instance = Point.rectangular(1.0, 1.0);
        double expected = Math.sqrt(2.0);
        double result = instance.getDistance(that);
        assertEquals(expected, result, 0.0);
    }
}
