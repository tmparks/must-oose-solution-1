
import java.util.List;
import org.junit.Test;
import static org.junit.Assert.*;

public class SimplePolygonTest
{

    /**
     * Test of rotate method, of class SimplePolygon.
     */
    @Test
    public void testRotate()
    {
        System.out.println("rotate");
        double angle = 17.0;
        SimplePolygon instance = new SimplePolygonImpl();
        instance.rotate(angle);
        assertEquals(angle, instance.getOrientation(), 0.0);
    }

    /**
     * Test of getOrientation method, of class SimplePolygon.
     */
    @Test
    public void testGetOrientation()
    {
        System.out.println("getOrientation");
        SimplePolygon instance = new SimplePolygonImpl();
        double expected = 0.0;
        double result = instance.getOrientation();
        assertEquals(expected, result, 0.0);
    }

    public class SimplePolygonImpl extends SimplePolygon
    {

        @Override
        public List<Point> getVertices()
        {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        @Override
        public void scale(double scale)
        {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        @Override
        public double getArea()
        {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }
    }

}
