
import org.junit.Test;
import static org.junit.Assert.*;

public class ShapeTest
{

    /**
     * Test of getCenter method, of class Shape.
     */
    @Test
    public void testGetCenter()
    {
        System.out.println("getCenter");
        double delta = 1e-6;
        Shape instance = new ShapeImpl();
        Point expected = new Point();
        Point result = instance.getCenter();
        assertEquals(0.0, result.getX(), delta);
        assertEquals(0.0, result.getY(), delta);
    }

    /**
     * Test of translate method, of class Shape.
     */
    @Test
    public void testTranslate()
    {
        System.out.println("translate");
        double x = 17.0;
        double y = -42.0;
        double delta = 1e-6;
        Shape instance = new ShapeImpl();
        instance.translate(x, y);
        Point result = instance.getCenter();
        assertEquals(x, result.getX(), delta);
        assertEquals(y, result.getY(), delta);
    }

    public class ShapeImpl extends Shape
    {

        @Override
        public void rotate(double angle)
        {
        }

        @Override
        public void scale(double scale)
        {
        }

        @Override
        public double getArea()
        {
            return 0.0;
        }

        @Override
        public double getPerimeter()
        {
            return 0.0;
        }

        @Override
        public Rectangle getBounds()
        {
            return null;
        }
    }

}
